#!/bin/bash

# {{ ansible_managed }}

docker run -d -p 80:80  --name=nginxletsencrypt \
    -v "{{ letsencrypt_certs_dir }}:{{ letsencrypt_certs_dir }}" \
    -v "{{ letsencrypt_acme_challenge_dir }}:{{ letsencrypt_acme_challenge_dir }}" \
    -v /tmp/nginxletsencrypt.conf:/etc/nginx/conf.d/nginxletsencrypt.conf \
    nginx:1.10.0

docker run -it --rm --name letsencrypt \
    -v "{{ letsencrypt_certs_dir }}:{{ letsencrypt_certs_dir }}" \
    -v "{{ letsencrypt_acme_challenge_dir }}:{{ letsencrypt_acme_challenge_dir }}" \
    quay.io/letsencrypt/letsencrypt:latest certonly --email {{ letsencrypt_email }} --webroot -w {{ letsencrypt_acme_challenge_dir }} --agree-tos -d {{ letsencrypt_domain_name }}
